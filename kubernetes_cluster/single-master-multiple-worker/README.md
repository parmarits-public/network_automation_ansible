# Deploy Kubernetes Cluster with Ansible

This Ansible playbook automates the deployment of a Kubernetes cluster with a single master node and multiple worker nodes. The playbook assumes the target hosts are running Ubuntu and you already have certificate based authenticaiton mechanism on all the machines.

## Usage

### Prerequisites

1. Ansible installed on your local machine.
2. SSH access to the target hosts.

### Configuration

1. Open the `inventory` file and replace the placeholders with the actual IP addresses or hostnames of your machines.
    ```ini
    [master]
    master-node ansible_host=<master-ip>

    [workers]
    worker-node1 ansible_host=<worker1-ip>
    worker-node2 ansible_host=<worker2-ip>
    # Add more worker nodes as needed
    ```

2. Adjust the `ansible_ssh_user` in the `ansible.cfg` file to the user with SSH access on your target hosts.

### Run the Ansible Playbook

Execute the following command to deploy the Kubernetes cluster:

```bash
ansible-playbook -i inventory deploy_kubernetes_cluster.yaml

## Ansible Playbook Tasks

This playbook will perform the following tasks:

- Set hostnames on all nodes.
- Import Kubernetes GPG key.
- Add Kubernetes APT repository.
- Install Docker and Kubernetes tools (kubeadm, kubelet, kubectl).
- Disable swap.
- Configure sysctl settings for Kubernetes.
- Configure Docker daemon.
- Initialize the master node using kubeadm.
- Configure kubectl on the master node.
- Deploy Flannel CNI on the master node.
- Generate join token for worker nodes.
- Execute join command on worker nodes.

**Note:** Ensure you have a backup and have validated compatibility before running this playbook in a production environment.

