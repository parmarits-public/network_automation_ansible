# Ansible Infrastructure Deployment

This Ansible playbook automates the deployment of VPN configurations on Palo Alto Networks firewalls. The playbook is organized into tasks for creating VPN configurations for two customers (`CUSTOMER 1` and `CUSTOMER 2`) on Palo Alto devices (`PALO-1` and `PALO-2`). Additionally, there is a task for committing the changes to all Palo Alto nodes.

## Prerequisites

1. Ensure Ansible is installed on the control machine.
2. Modify the `provider.yml` file with the correct management IP addresses, usernames, and API keys for your Palo Alto devices.

   ```yaml
   palo_provider:
     ip_address: "{{ ansible_host }}"
     username: "admin"
     api_key: "YOUR_API_KEY"

Replace "YOUR_API_KEY" with the appropriate API key generated for each Palo Alto device. Avoid exposing sensitive information like API keys in the code; consider using a secure method, such as a secret manager.

## How to Run the Playbook
* Clone the repository containing your Ansible code.
* Navigate to the directory where the Ansible code is located.
* Update your host file with the IP addresses of Firewalls
* Modify the deployinfra.yml file with the variables as agreed between you and the peer end
* Run the Ansible playbook.
   ```yaml
   ansible-playbook deployinfra.yml

## Module Files
The modules directory contains the following module files:
1. zone.yml: For creating zones
2. ipsecvpn.yml: For creating IPSec VPN

## Important Notes
API Key Security:

Avoid exposing API keys in the code. For security reasons, generate API keys securely and consider using a secret manager to fetch them during playbook execution.
VPN Configurations:

Review and customize the VPN configuration parameters in the playbook according to your specific requirements.
## Committing Changes:

The playbook includes a task (COMMITING THE CHANGES TO FIREWALLS) for committing changes to the Palo Alto firewalls. Ensure that this step aligns with your change management process.

## Testing:

Before deploying to a production environment, thoroughly test the playbook in a controlled environment to validate the configurations.
Documentation:

Refer to the documentation of the Palo Alto Networks Ansible collection for additional details on module parameters and best practices: paloaltonetworks.panos Collection Documentation
Feel free to reach out for any further assistance or customization needs related to this Ansible playbook.